class InfoModel {
  String? message;
  String? field;
  String? redirect;

  InfoModel({this.message, this.field, this.redirect});

  InfoModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    field = json['field'];
    redirect = json['redirect'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['field'] = this.field;
    data['redirect'] = this.redirect;
    return data;
  }
}
