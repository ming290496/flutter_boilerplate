import 'package:flutter_boilerplate/src/models/info_model.dart';

class GlobalDefaultResponseModel {
  int? status;
  bool? acknowledge;
  InfoModel? info;

  // Null? data;

  GlobalDefaultResponseModel({this.status, this.acknowledge, this.info});

  GlobalDefaultResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    acknowledge = json['acknowledge'];
    // data = json['data'];
    info = json['info'] != null ? new InfoModel.fromJson(json['info']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['acknowledge'] = this.acknowledge;
    // data['data'] = this.data;
    if (this.info != null) {
      data['info'] = this.info?.toJson();
    }
    return data;
  }

  static Map<String, dynamic> generateParams(String? exampleParameter) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['example_parameter'] = exampleParameter;
    return data;
  }
}
