import 'package:flutter_boilerplate/src/models/info_model.dart';

class FeedListResponseModel {
  int? status;
  bool? acknowledge;
  List<FeedModel>? data;
  InfoModel? info;

  FeedListResponseModel({this.status, this.acknowledge, this.data, this.info});

  FeedListResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    acknowledge = json['acknowledge'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data!.add(new FeedModel.fromJson(v));
      });
    }
    info = json['info'] != null ? new InfoModel.fromJson(json['info']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['acknowledge'] = this.acknowledge;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    if (this.info != null) {
      data['info'] = this.info!.toJson();
    }
    return data;
  }
}

class FeedModel {
  int? id;
  String? image;
  String? authorName;
  bool? isLoved;
  bool? isSaved;
  String? totalLikesText;
  String? caption;
  int? totalComments;
  String? timeStamp;

  FeedModel(
      {this.id,
      this.image,
      this.authorName,
      this.isLoved,
      this.isSaved,
      this.totalLikesText,
      this.caption,
      this.totalComments,
      this.timeStamp});

  FeedModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    authorName = json['author_name'];
    isLoved = json['is_loved'];
    isSaved = json['is_saved'];
    totalLikesText = json['total_likes_text'];
    caption = json['caption'];
    totalComments = json['total_comments'];
    timeStamp = json['time_stamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['author_name'] = this.authorName;
    data['is_loved'] = this.isLoved;
    data['is_saved'] = this.isSaved;
    data['total_likes_text'] = this.totalLikesText;
    data['caption'] = this.caption;
    data['total_comments'] = this.totalComments;
    data['time_stamp'] = this.timeStamp;
    return data;
  }
}
