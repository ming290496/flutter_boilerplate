import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/constants/gl_text_style.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_sized_box.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';

class Utilities {
  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  static Future<PackageInfo> getPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    return info;
  }

  static Future<void> dismissKeyboard(BuildContext context) async {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    await Utilities.addDelay(milliseconds: 300);
  }

  static showSnackBar(context, String title) {
    final snackBar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: GLColor.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: Colors.red,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static Future<void> addDelay(
      {int days = 0,
      int hours = 0,
      int minutes = 0,
      int seconds = 0,
      int milliseconds = 0,
      int microseconds = 0}) async {
    await Future.delayed(
        Duration(
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds,
            milliseconds: milliseconds,
            microseconds: microseconds),
        () {});
  }

  static void showToast(String msg, {Toast toastLength = Toast.LENGTH_LONG}) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: toastLength,
      timeInSecForIosWeb: 3,
    );
  }

  static String formatDate(String date,
      {String format = 'EEEE, dd MMMM yyyy, hh:mm aa'}) {
    try {
      initializeDateFormatting();
      return DateFormat(format, 'id').format(DateTime.parse(date));
    } catch (e) {
      debugPrint(e.toString());
      return '-';
    }
  }

  static showBottomSheet(
    context, {
    required String title,
    required Widget widget,
    bool isDismissible = true,
    bool enableDrag = true,
    Color backgroundColor = Colors.white,
  }) async {
    await showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      isScrollControlled: true,
      isDismissible: isDismissible,
      enableDrag: enableDrag,
      builder: (_) => WillPopScope(
        onWillPop: () async => isDismissible,
        child: Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(16),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GLSizedBox.spaceSectionVertical(),
                    Text(
                      title,
                      style: GLTextStyle.bottomSheetTitle,
                      textAlign: TextAlign.center,
                    ),
                    GLSizedBox.spaceSectionVertical(),
                    widget,
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Future<void> showPopupCustom(BuildContext context,
      {required Widget widget, bool isDismissible = true}) async {
    await showDialog(
      context: context,
      builder: (_) {
        return WillPopScope(
          onWillPop: () async => isDismissible,
          child: Center(
            child: Material(
              color: Colors.transparent,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                margin: const EdgeInsets.symmetric(horizontal: 40),
                decoration: BoxDecoration(
                    color: GLColor.buttonEnabled,
                    borderRadius: BorderRadius.circular(10.0)),
                child: widget,
              ),
            ),
          ),
        );
      },
      barrierDismissible: isDismissible,
    );
  }

  static Future<void> showPopupCustomWithTitle(BuildContext context,
      {required Widget widget,
      required String title,
      bool isDismissible = true}) async {
    await showDialog(
      context: context,
      builder: (_) {
        return _SystemPadding(
          child: WillPopScope(
            onWillPop: () async => isDismissible,
            child: Center(
              child: Material(
                color: Colors.transparent,
                child: Container(
                  // padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: const EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                      color: GLColor.white,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: GLColor.primaryColor,
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            )),
                        padding: const EdgeInsets.all(16),
                        child: Text(
                          title,
                          textAlign: TextAlign.center,
                          style: GLTextStyle.bottomSheetTitle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: widget,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      barrierDismissible: isDismissible,
    );
  }

  static Future<void> showPopupConfirmation(
    BuildContext context, {
    required String title,
    required String description,
    required Function btnPositiveClicked,
    required Function btnNegativeClicked,
    required String btnPositiveText,
    required String btnNegativeText,
  }) async {
    await showDialog(
      context: context,
      builder: (_) {
        return WillPopScope(
          onWillPop: () async => true,
          child: Center(
            child: Material(
              color: Colors.transparent,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                margin: const EdgeInsets.symmetric(horizontal: 40),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      title,
                      style: GLTextStyle.sectionTitle
                          .copyWith(color: GLColor.black),
                      textAlign: TextAlign.center,
                    ),
                    GLSizedBox.spaceSectionVertical(),
                    Text(
                      description,
                      style: GLTextStyle.sectionSubTitle
                          .copyWith(color: GLColor.black),
                      textAlign: TextAlign.center,
                    ),
                    GLSizedBox.spaceSectionVertical(),
                    const Divider(),
                    // TODO: IntrinsicHeight is Expensive
                    IntrinsicHeight(
                      child: Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                btnNegativeClicked();
                              },
                              child: Text(
                                btnNegativeText,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          const VerticalDivider(),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                btnPositiveClicked();
                              },
                              child: Text(
                                btnPositiveText,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
      barrierDismissible: false,
    );
  }

  static String calcDoubleToString(double val) {
    RegExp regex = RegExp(r"([.]*0)(?!.*\d)");

    String s = val.toString().replaceAll(regex, "");
    return s;
  }

  static void forceLogOut() async {
    print('force log out');
    // await PreferencesHelper.logout();
    // navigatorKey.currentState.pushNamedAndRemoveUntil(
    //     RegisterScreen.routeName, (Route<dynamic> route) => false);
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  const _SystemPadding({required this.child});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
