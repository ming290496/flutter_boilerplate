import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/src/blocs/bloc_provider.dart';
import 'package:flutter_boilerplate/src/blocs/root/root_bloc.dart';
import 'package:flutter_boilerplate/src/blocs/splash/splash_bloc.dart';
import 'package:flutter_boilerplate/src/constants/gl_app_const.dart';
import 'package:flutter_boilerplate/src/screens/root/root_screen.dart';
import 'package:flutter_boilerplate/src/screens/splash/splash_screen.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    precacheImage(const AssetImage("assets/images/ic_logo.png"), context);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));

    return MaterialApp(
      title: 'Flutter Boilerplate',
      theme: ThemeData(
        fontFamily: GLAppConst.defaultFont,
      ),
      onGenerateRoute: routes,
      initialRoute: SplashScreen.routeName,
    );
  }

  static Route<dynamic> routes(RouteSettings settings) {
    switch (settings.name) {
      case SplashScreen.routeName:
        final bloc = SplashBloc();
        // final argument = settings.arguments;
        return MaterialPageRoute(
          builder: (context) {
            return BlocProvider<SplashBloc>(
              bloc: bloc,
              child: WillPopScope(
                onWillPop: () async => false,
                child: const SplashScreen(),
              ),
            );
          },
        );
      case RootScreen.routeName:
        final bloc = RootBloc();
        // final argument = settings.arguments;
        return MaterialPageRoute(
          builder: (context) {
            return BlocProvider<RootBloc>(
              bloc: bloc,
              child: WillPopScope(
                onWillPop: () async => false,
                child: const RootScreen(),
              ),
            );
          },
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
