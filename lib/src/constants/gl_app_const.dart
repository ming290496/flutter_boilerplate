class GLAppConst {
  static const String defaultFont = 'Montserrat';
  static const String lorem = 'lorem';
  static const double normalSize = 16;
  static const String errorText = 'Oops! Something Went Wrong...';
}
