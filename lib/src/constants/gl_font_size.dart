class GLFontSize {
  static double get h1 => 32.0;

  static double get h2 => 24.0;

  static double get h3 => 18.72;

  static double get h4 => 16;

  static double get h5 => 13.28;

  static double get h6 => 10.72;

  static double get logoTextSize => 30.0;
}
