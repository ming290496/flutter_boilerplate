import 'package:flutter/material.dart';

class GLColor {
  static Color get transparent => Colors.transparent;

  static Color get backgroundColor => Colors.white;

  // static Color get backgroundColor => const Color(0xFF212121);

  static Color get backgroundGreyColor => const Color(0xFF454545);

  static Color get primaryColor => const Color(0xFF262626);

  static Color get secondaryColor => const Color(0xFFc7c7c7);

  static Color get errorColor => const Color(0xFFFF9B9B);

  static Color get white => Colors.white;

  static Color get black => Colors.black;

  static Color get darkGrey => Colors.black45;

  static Color get buttonEnabled => const Color(0xFFFFDD00);

  static Color get buttonDisabled => const Color(0xFFFFDD00).withOpacity(0.8);

  static Color get placeholder => const Color(0xFFc7c7c7);

  static Color get red => const Color(0xFFFF0000);
}
