import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_app_const.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/constants/gl_font_size.dart';
import 'package:flutter_boilerplate/src/constants/gl_font_weight.dart';

class GLTextStyle {
  static TextStyle get defaultText => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.primaryColor,
        fontSize: GLFontSize.h4,
      );

  static TextStyle get errorText => defaultText.copyWith(
        fontSize: GLFontSize.h5,
      );

  static TextStyle get sectionTitle => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.secondaryColor,
        fontSize: GLFontSize.h4,
        fontWeight: GLFontWeight.semiBold,
      );

  static TextStyle get sectionSubTitle => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.secondaryColor,
        fontSize: GLFontSize.h5,
      );

  static TextStyle get appBarTitle => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.primaryColor,
      );

  static TextStyle get versionName => TextStyle(
        color: GLColor.darkGrey,
        fontSize: GLFontSize.h5,
        fontWeight: GLFontWeight.medium,
      );

  static TextStyle get buttonStyle => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.black,
        fontSize: GLFontSize.h4,
        fontWeight: GLFontWeight.semiBold,
      );

  static TextStyle get bottomSheetTitle => TextStyle(
        fontFamily: GLAppConst.defaultFont,
        color: GLColor.black,
        fontSize: GLFontSize.h4,
        fontWeight: GLFontWeight.semiBold,
      );

  static TextStyle get bottomSheetText => defaultText.copyWith(
        fontWeight: GLFontWeight.medium,
      );

  ///FEED
  static TextStyle get feedTitle => defaultText.copyWith(
        fontWeight: GLFontWeight.medium,
        fontSize: GLFontSize.h5,
        color: GLColor.black,
      );

  static TextStyle get feedCaption => defaultText.copyWith();

  static TextStyle get feedCommentButton => defaultText.copyWith(
        fontWeight: GLFontWeight.medium,
        fontSize: GLFontSize.h5,
        color: GLColor.placeholder,
      );

  static TextStyle get feedTimeStamp => defaultText.copyWith(
        fontWeight: GLFontWeight.normal,
        fontSize: GLFontSize.h5,
        color: GLColor.placeholder,
      );
}
