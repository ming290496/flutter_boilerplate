import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/constants/gl_text_style.dart';
import 'package:flutter_boilerplate/src/models/feed/feed_list_response_model.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_sized_box.dart';

class FeedWidget extends StatelessWidget {
  final FeedModel item;

  FeedWidget({required this.item});

  @override
  Widget build(BuildContext context) {
    final bool isLoved = item.isLoved ?? false;
    final bool isSaved = item.isSaved ?? false;

    return Container(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 25,
                backgroundColor: GLColor.placeholder,
                backgroundImage: NetworkImage(item.image ?? ''),
              ),
              GLSizedBox.spaceListHorizontal(),
              Expanded(
                child: Text(
                  item.authorName ?? '',
                  style: GLTextStyle.feedTitle,
                ),
              ),
              PopupMenuButton<String>(
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.more_vert,
                  ),
                ),
                onSelected: (value) {
                  switch (value) {
                    case 'Share':
                      break;
                    case 'Report':
                      break;
                  }
                },
                itemBuilder: (BuildContext context) {
                  return {'Share', 'Report'}.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              ),
            ],
          ),
          GLSizedBox.spaceListVertical(),
          Container(
            child: AspectRatio(
              aspectRatio: 1,
              child: Image.network(item.image ?? ''),
            ),
            color: GLColor.placeholder,
          ),
          GLSizedBox.spaceListVertical(),
          Row(
            children: [
              Icon(
                isLoved ? Icons.favorite : Icons.favorite_border,
                color: GLColor.red,
              ),
              GLSizedBox.spaceListHorizontal(),
              Icon(
                Icons.chat_bubble_outline,
              ),
              GLSizedBox.spaceListHorizontal(),
              Icon(
                Icons.send,
              ),
              Expanded(child: Container()),
              Icon(
                isSaved ? Icons.bookmark : Icons.bookmark_outline,
              ),
            ],
          ),
          GLSizedBox.spaceListVertical(),
          Text(
            item.totalLikesText ?? '',
            style: GLTextStyle.feedTitle,
          ),
          RichText(
            text: TextSpan(
              style: GLTextStyle.feedCaption,
              children: <TextSpan>[
                TextSpan(text: 'Username ', style: GLTextStyle.feedTitle),
                TextSpan(text: item.caption ?? ''),
              ],
            ),
          ),
          Text(
            'View all ${item.totalComments ?? 0} comments',
            style: GLTextStyle.feedCommentButton,
          ),
          Text(
            item.timeStamp ?? '',
            style: GLTextStyle.feedTimeStamp,
          ),
        ],
      ),
    );
  }
}
