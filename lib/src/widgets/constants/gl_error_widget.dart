import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_app_const.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/constants/gl_text_style.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_sized_box.dart';

class GLErrorWidget extends StatelessWidget {
  final String? errorText;

  GLErrorWidget(this.errorText);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Icon(
              Icons.cancel_outlined,
              color: GLColor.errorColor,
              size: 100,
            ),
            GLSizedBox.spaceSectionVertical(
              height: 10,
            ),
            Text(
              errorText ?? GLAppConst.errorText,
              textAlign: TextAlign.center,
              style: GLTextStyle.errorText,
            )
          ],
        ));
    ;
  }
}
