import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class GLLoadingSpinner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SpinKitFadingCircle(
          color: GLColor.errorColor,
          size: 50.0,
        ),
      ),
    );
  }
}
