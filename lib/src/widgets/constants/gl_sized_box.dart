import 'package:flutter/material.dart';

class GLSizedBox extends StatelessWidget {
  final double width;
  final double height;

  GLSizedBox({this.width = 0, this.height = 0});

  /// CUSTOM

  GLSizedBox.spaceSectionVertical({this.width = 0, this.height = 18});

  GLSizedBox.spaceListVertical({this.width = 0, this.height = 10});

  GLSizedBox.spaceSectionHorizontal({this.width = 18, this.height = 0});

  GLSizedBox.spaceListHorizontal({this.width = 10, this.height = 0});

  /// VERTICAL

  GLSizedBox.xxxlVertical({this.width = 0, this.height = 45});

  GLSizedBox.xxlVertical({this.width = 0, this.height = 40});

  GLSizedBox.xlVertical({this.width = 0, this.height = 35});

  GLSizedBox.lVertical({this.width = 0, this.height = 30});

  GLSizedBox.normalVertical({this.width = 0, this.height = 25});

  GLSizedBox.sVertical({this.width = 0, this.height = 20});

  GLSizedBox.xsVertical({this.width = 0, this.height = 15});

  GLSizedBox.xxsVertical({this.width = 0, this.height = 10});

  GLSizedBox.xxxsVertical({this.width = 0, this.height = 5});

  /// HORIZONTAL

  GLSizedBox.xxxlHorizontal({this.width = 45, this.height = 0});

  GLSizedBox.xxlHorizontal({this.width = 40, this.height = 0});

  GLSizedBox.xlHorizontal({this.width = 35, this.height = 0});

  GLSizedBox.lHorizontal({this.width = 30, this.height = 0});

  GLSizedBox.normalHorizontal({this.width = 25, this.height = 0});

  GLSizedBox.sHorizontal({this.width = 20, this.height = 0});

  GLSizedBox.xsHorizontal({this.width = 15, this.height = 0});

  GLSizedBox.xxsHorizontal({this.width = 10, this.height = 0});

  GLSizedBox.xxxsHorizontal({this.width = 5, this.height = 0});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
    );
  }
}
