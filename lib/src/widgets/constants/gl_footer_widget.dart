import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_text_style.dart';
import 'package:flutter_boilerplate/src/utilities/utilities.dart';
import 'package:package_info/package_info.dart';

class GLFooterWidget extends StatelessWidget {
  const GLFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16, top: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FutureBuilder<PackageInfo>(
            future: Utilities.getPackageInfo(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final data = snapshot.data;
              final version = data?.version ?? '';

              return Text('Flutter Boilerplate v$version\nKonawei \u00a9 2021',
                  textAlign: TextAlign.center, style: GLTextStyle.versionName);
            },
          ),
        ],
      ),
    );
  }
}
