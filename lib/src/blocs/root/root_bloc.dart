import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/blocs/bloc.dart';
import 'package:flutter_boilerplate/src/constants/gl_enum.dart';
import 'package:rxdart/rxdart.dart';

class RootBloc implements Bloc {
  @override
  late BuildContext context;

  ///INITIAL
  final _navBar = BehaviorSubject<NavBarEnum>();

  ///STREAM
  Stream<NavBarEnum> get navBar => _navBar.stream;

  ///FUNCTION
  Function(NavBarEnum) get addNavBar => _navBar.sink.add;

  ///VALUE
  NavBarEnum get navBarValue => _navBar.value;

  ///
  pickItem(index) {
    switch (index) {
      case 0:
        addNavBar(NavBarEnum.HOME);
        break;
      case 1:
        addNavBar(NavBarEnum.BROWSE);
        break;
      case 2:
        addNavBar(NavBarEnum.PROFILE);
        break;
    }
  }

  int getItem(NavBarEnum navBarEnum) {
    switch (navBarEnum) {
      case NavBarEnum.HOME:
        return 0;
      case NavBarEnum.BROWSE:
        return 1;
      case NavBarEnum.PROFILE:
        return 2;
      default:
        return 0;
    }
  }

  changeItem(NavBarEnum navBarEnum) {
    switch (navBarEnum) {
      case NavBarEnum.HOME:
        break;
      case NavBarEnum.BROWSE:
        break;
      case NavBarEnum.PROFILE:
        break;
    }
  }

  @override
  void dispose() {
    _navBar.close();
  }
}
