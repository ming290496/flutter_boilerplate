import 'package:flutter/material.dart';

abstract class Bloc {
  late BuildContext context;

  void dispose();
}
