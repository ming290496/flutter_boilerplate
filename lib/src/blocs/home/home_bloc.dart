import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/blocs/bloc.dart';
import 'package:flutter_boilerplate/src/models/feed/feed_list_response_model.dart';
import 'package:flutter_boilerplate/src/resources/global_repository.dart';
import 'package:flutter_boilerplate/src/resources/response_state.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc implements Bloc {
  @override
  late BuildContext context;
  final _globalRepo = GlobalRepository();

  final _feedList = BehaviorSubject<ApiResponse<List<FeedModel>>>();

  Stream<ApiResponse<List<FeedModel>>> get feedList => _feedList.stream;

  Function(ApiResponse<List<FeedModel>>) get addFeedList => _feedList.sink.add;

  HomeBloc() {
    getData();
  }

  getData() async {
    try {
      addFeedList(ApiResponse.loading());
      final response = await _globalRepo.feedList();
      addFeedList(ApiResponse.completed(response.data));
    } catch (e, s) {
      print(s);
      addFeedList(ApiResponse.error(e.toString()));
    }
  }

  @override
  void dispose() {
    _feedList.close();
  }
}
