import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/blocs/bloc.dart';
import 'package:flutter_boilerplate/src/screens/root/root_screen.dart';
import 'package:flutter_boilerplate/src/utilities/utilities.dart';

class SplashBloc implements Bloc {
  @override
  late BuildContext context;

  SplashBloc() {
    getData();
  }

  getData() async {
    await Utilities.addDelay(seconds: 3);
    Navigator.pushNamed(context, RootScreen.routeName);
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
