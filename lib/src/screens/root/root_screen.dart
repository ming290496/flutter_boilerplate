import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/src/blocs/bloc_provider.dart';
import 'package:flutter_boilerplate/src/blocs/browse/browse_bloc.dart';
import 'package:flutter_boilerplate/src/blocs/home/home_bloc.dart';
import 'package:flutter_boilerplate/src/blocs/profile/profile_bloc.dart';
import 'package:flutter_boilerplate/src/blocs/root/root_bloc.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/constants/gl_enum.dart';
import 'package:flutter_boilerplate/src/screens/browse/browse_screen.dart';
import 'package:flutter_boilerplate/src/screens/home/home_screen.dart';
import 'package:flutter_boilerplate/src/screens/profile/profile_screen.dart';
import 'package:flutter_boilerplate/src/utilities/utilities.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_sized_box.dart';

class RootScreen extends StatefulWidget {
  static const String routeName = '/root';

  const RootScreen({Key? key}) : super(key: key);

  @override
  _RootScreenState createState() => _RootScreenState();
}

class _RootScreenState extends State<RootScreen>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  late final HomeBloc homeBloc;
  late final Widget homeScreen;

  late final BrowseBloc browseBloc;
  late final Widget browseScreen;

  late final ProfileBloc profileBloc;
  late final Widget profileScreen;

  @override
  void initState() {
    homeBloc = HomeBloc();
    homeScreen = BlocProvider<HomeBloc>(
      bloc: homeBloc,
      child: HomeScreen(),
    );

    browseBloc = BrowseBloc();
    browseScreen = BlocProvider<BrowseBloc>(
      bloc: browseBloc,
      child: BrowseScreen(),
    );

    profileBloc = ProfileBloc();
    profileScreen = BlocProvider<ProfileBloc>(
      bloc: profileBloc,
      child: ProfileScreen(),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<RootBloc>(context);

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: GLColor.backgroundColor,
          foregroundColor: GLColor.primaryColor,
          centerTitle: true,
          title: const Text('Flutter Boilerplate'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.camera_alt_outlined),
            onPressed: () {},
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.favorite_border),
              onPressed: () {},
            ),
            IconButton(
              icon: const Icon(Icons.send),
              onPressed: () {},
            ),
          ],
        ),
        backgroundColor: GLColor.backgroundColor,
        body: SafeArea(
          child: StreamBuilder<NavBarEnum>(
              stream: bloc.navBar,
              initialData: NavBarEnum.HOME,
              builder: (context, snapshot) {
                final data = snapshot.data ?? bloc.navBarValue;

                switch (data) {
                  case NavBarEnum.HOME:
                    return homeScreen;
                  case NavBarEnum.BROWSE:
                    return browseScreen;
                  case NavBarEnum.PROFILE:
                    return profileScreen;
                }
              }),
        ),
        bottomNavigationBar: StreamBuilder<NavBarEnum>(
          stream: bloc.navBar,
          initialData: NavBarEnum.HOME,
          builder: (context, snapshot) {
            final data = snapshot.data ?? bloc.navBarValue;
            return BottomNavigationBar(
              backgroundColor: GLColor.backgroundColor,
              selectedItemColor: GLColor.primaryColor,
              unselectedItemColor: GLColor.secondaryColor,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              onTap: (value) async {
                bloc.pickItem(value);
                //
                // ///IF HOME CLICK
                // if (value == 0) {
                //   homeBloc.getData();
                // }
              },
              currentIndex: bloc.getItem(data),
              items: const [
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                    size: kToolbarHeight * 0.75,
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.search,
                    size: kToolbarHeight * 0.75,
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.person,
                    size: kToolbarHeight * 0.75,
                  ),
                  label: '',
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  DateTime? currentBackPressTime;

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Utilities.showToast('Press back again to exit');
      return Future.value(false);
    }
    try {
      SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
    } catch (e) {}
    return Future.value(false);
  }
}
