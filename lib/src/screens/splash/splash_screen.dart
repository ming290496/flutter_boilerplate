import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/constants/gl_color.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_footer_widget.dart';

class SplashScreen extends StatelessWidget {
  static const String routeName = '/';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GLColor.backgroundColor,
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 130.0),
                        child: Image.asset(
                          'assets/images/ic_logo.png',
                        ),
                      ),
                    ],
                  ),
                ),
                const GLFooterWidget(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
