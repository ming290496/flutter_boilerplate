import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/blocs/bloc_provider.dart';
import 'package:flutter_boilerplate/src/blocs/home/home_bloc.dart';
import 'package:flutter_boilerplate/src/constants/gl_app_const.dart';
import 'package:flutter_boilerplate/src/constants/gl_padding.dart';
import 'package:flutter_boilerplate/src/models/feed/feed_list_response_model.dart';
import 'package:flutter_boilerplate/src/resources/response_state.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_error_widget.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_loading_spinner.dart';
import 'package:flutter_boilerplate/src/widgets/constants/gl_sized_box.dart';
import 'package:flutter_boilerplate/src/widgets/feed/feed_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeBloc>(context);
    return StreamBuilder<ApiResponse<List<FeedModel>>>(
      stream: bloc.feedList,
      builder: (context, snapshot) {
        final data = snapshot.data;

        switch (data?.status) {
          case Status.LOADING:
            return GLLoadingSpinner();
          case Status.COMPLETED:
            final feed = data?.data ?? [];
            return ListView.separated(
                padding: GLPadding.defaultVerticalList,
                itemCount: feed.length,
                separatorBuilder: (c, i) {
                  return GLSizedBox.spaceListVertical();
                },
                itemBuilder: (c, i) {
                  return FeedWidget(
                    item: feed[i],
                  );
                });
          case Status.ERROR:
            return GLErrorWidget(data?.message);
          default:
            return Container();
        }
      },
    );
  }
}
