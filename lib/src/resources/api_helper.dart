import 'dart:io';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter_boilerplate/src/utilities/app_exception.dart';
import 'package:flutter_boilerplate/src/utilities/utilities.dart';
import 'package:http/http.dart' as client;
import 'dart:async';

import 'package:flutter_boilerplate/src/resources/configs.dart';

class ApiHelper {
  ApiHelper._instantiate();

  List<String> baseUrl = getBaseUrl();

  static final ApiHelper instance = ApiHelper._instantiate();

  var header = new Map<String, String>();

  ApiHelper();

  static List<String> getBaseUrl() {
    return Configs.baseUrl;
  }

  static String _getAuthorization() {
    return Configs.authorization;
  }

  static Duration _getTimeOutDuration() {
    return Configs.timeOutDuration;
  }

  Future<Map<String, String>> getHeader() async {
    var result = Map<String, String>();

    result["Authorization"] = _getAuthorization();
    result["PackageName"] = getPackageName();
    result["Content-Type"] = "application/json";
    result["MobileDevice"] = getMobileDevice();

    print('header : $result');
    return result;
  }

  Future<Map<String, String>> getHeaderUrlEncodes() async {
    var result = await getHeader();
    result["Content-Type"] = "application/x-www-form-urlencoded";

    print('header : $result');
    return result;
  }

  String getPackageName() {
    String result = '';
    if (Platform.isAndroid) {
      result = 'com.flutter_boilerplate.android';
    } else {
      result = 'com.flutter_boilerplate.ios';
    }
    return result;
  }

  String getMobileDevice() {
    String result = '';
    if (Platform.isAndroid) {
      result = 'android';
    } else {
      result = 'ios';
    }
    return result;
  }


  Future<dynamic> getMocky(String url) async {
    print('Api GetMocky url $url');

    String _url = url;

    var responseJson;
    // final HttpMetric metric =
    // FirebasePerformance.instance.newHttpMetric(_url, HttpMethod.Get);
    // await metric.start();
    // print('METRIC START');

    try {
      final response = await client
          .get(Uri.parse(_url), headers: await getHeader())
          .timeout(_getTimeOutDuration());
      // metric
      //   ..requestPayloadSize = 0
      //   ..responsePayloadSize = response.contentLength
      //   ..responseContentType =
      //       response.headers['Content-Type'] ?? response.headers['content-type']
      //   ..httpResponseCode = response.statusCode;
      // print('requestPayloadSize: ${metric.requestPayloadSize}');
      // print('responsePayloadSize: ${metric.responsePayloadSize}');
      // print('responseContentType: ${metric.responseContentType}');
      // print('httpResponseCode: ${metric.httpResponseCode}');

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print('Oops! Something Went Wrong...');
      throw FetchDataException('Oops! Something Went Wrong...');
    } finally {
      // await metric.stop();
    }

    print('api get recieved!');
    return responseJson;
  }

  Future<dynamic> get(String url,
      {dynamic params, int baseUrlIndex = 0}) async {
    print('Api Get, url ${baseUrl[baseUrlIndex] + url}');
    if (params != null) {
      url += mapToParamString(params);
    }
    print(url);
    String _url = baseUrl[baseUrlIndex] + url;

    var responseJson;
    // final HttpMetric metric =
    // FirebasePerformance.instance.newHttpMetric(_url, HttpMethod.Get);
    // await metric.start();
    // print('METRIC START');

    try {
      final response = await client
          .get(Uri.parse(_url), headers: await getHeader())
          .timeout(_getTimeOutDuration());
      // metric
      //   ..requestPayloadSize = 0
      //   ..responsePayloadSize = response.contentLength
      //   ..responseContentType =
      //       response.headers['Content-Type'] ?? response.headers['content-type']
      //   ..httpResponseCode = response.statusCode;
      // print('requestPayloadSize: ${metric.requestPayloadSize}');
      // print('responsePayloadSize: ${metric.responsePayloadSize}');
      // print('responseContentType: ${metric.responseContentType}');
      // print('httpResponseCode: ${metric.httpResponseCode}');

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print('Oops! Something Went Wrong...');
      throw FetchDataException('Oops! Something Went Wrong...');
    } finally {
      // await metric.stop();
    }

    print('api get recieved!');
    return responseJson;
  }

  Future<dynamic> post(String url,
      {dynamic params, int baseUrlIndex = 0}) async {
    print('Api Post, url ${baseUrl[baseUrlIndex] + url}');
    String bodyString = '';
    if (params != null) {
//      params.removeWhere((String key, dynamic value) => value == null);
      bodyString = jsonEncode(params);
    }
    print('Params : $bodyString');
    // log('Params : $bodyString');

    String _url = baseUrl[baseUrlIndex] + url;

    var responseJson;
    // final HttpMetric metric =
    // FirebasePerformance.instance.newHttpMetric(_url, HttpMethod.Post);
    // await metric.start();
    // print('METRIC START');

    try {
      final response = await client
          .post(Uri.parse(_url), body: bodyString, headers: await getHeader())
          .timeout(_getTimeOutDuration());
      // metric
      //   ..requestPayloadSize = bodyString.length
      //   ..responsePayloadSize = response.contentLength
      //   ..responseContentType =
      //       response.headers['Content-Type'] ?? response.headers['content-type']
      //   ..httpResponseCode = response.statusCode;
      // print('requestPayloadSize: ${metric.requestPayloadSize}');
      // print('responsePayloadSize: ${metric.responsePayloadSize}');
      // print('responseContentType: ${metric.responseContentType}');
      // print('httpResponseCode: ${metric.httpResponseCode}');

      print('RESPONSE ${response.body}');
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on TimeoutException {
      print('Oops! Something Went Wrong...');
      throw FetchDataException('Oops! Something Went Wrong...');
    } finally {
      // await metric.stop();
    }

    return responseJson;
  }

  // //POST
  // Future<dynamic> postWithImage(
  //     {String subCategoryId,
  //       String title,
  //       String description,
  //       List<String> imagePath}) async {
  //   String _url = baseUrl[1] + 'urlurl';
  //
  //   var responseJson;
  //   final HttpMetric metric =
  //   FirebasePerformance.instance.newHttpMetric(_url, HttpMethod.Post);
  //   await metric.start();
  //   print('METRIC START');
  //
  //   try {
  //     print('Api Post, url $_url');
  //     var request = client.MultipartRequest("POST", Uri.parse(_url));
  //     request.fields['sub_category_id'] = subCategoryId;
  //     request.fields['title'] = title;
  //     request.fields['description'] = description;
  //
  //     int arr = 0;
  //     if ((imagePath ?? '') != '') {
  //       imagePath.forEach((element) async {
  //         request.files.add(
  //             await client.MultipartFile.fromPath('file[]', imagePath[arr++]));
  //       });
  //     }
  //
  //     Map<String, String> headers = await getHeaderUrlEncodes();
  //     headers["Content-Type"] = "multipart/form-data";
  //     request.headers.addAll(headers);
  //
  //     final response = await request.send();
  //     final responseBody = await client.Response.fromStream(response);
  //     metric
  //       ..requestPayloadSize = request.contentLength
  //       ..responsePayloadSize = response.contentLength
  //       ..responseContentType =
  //           response.headers['Content-Type'] ?? response.headers['content-type']
  //       ..httpResponseCode = response.statusCode;
  //     print('requestPayloadSize: ${metric.requestPayloadSize}');
  //     print('responsePayloadSize: ${metric.responsePayloadSize}');
  //     print('responseContentType: ${metric.responseContentType}');
  //     print('httpResponseCode: ${metric.httpResponseCode}');
  //
  //     responseJson = _returnResponse(responseBody);
  //   } on SocketException {
  //     throw FetchDataException('No Internet Connection');
  //   } finally {
  //     await metric.stop();
  //   }
  //
  //   return responseJson;
  // }

  String mapToParamString(dynamic params) {
    String result = '?';
    params.forEach((a, b) {
      if (a == 'week' && b == '0') {
        result += '$a=$b&';
        return;
      }
      if (b != null && b != '' && (b != '0' || a == 'page')) result += '$a=$b&';
    });
    return result;
  }

  dynamic _returnResponse(client.Response response) {
    //TODO: Bikin force logout dari utilities force logout
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        if (responseJson["acknowledge"] == false ||
            responseJson["status"].toString() == 'error') {
          if (responseJson["info"] != null) {
            if ((responseJson["info"]["redirect"] ?? '') == 'login' ||
                (responseJson["info"]["field"] ?? '')
                    .toString()
                    .toLowerCase()
                    .contains('session')) {
              Utilities.forceLogOut();
            } else {
              throw Exception(responseJson["info"]["message"] ?? '');
            }
          } else {
            throw Exception('Oops! Something Went Wrong...');
          }
        }
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 413:
        throw UnauthorisedException(
            'Error occured while Communication with Server.');
      case 500:
      case 502:
      // throw SocketException('Oops! Something Went Wrong...');
      default:
        throw FetchDataException(
            'Error occured while Communication with Server.\nStatusCode :${response.statusCode}');
    }
  }

  injectParam(var request, String key, dynamic value) {
    if (value != null) request.fields['$key'] = value;
  }
}
