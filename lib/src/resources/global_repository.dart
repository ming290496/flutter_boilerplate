import 'package:flutter_boilerplate/src/models/feed/feed_list_response_model.dart';
import 'package:flutter_boilerplate/src/models/global_default_response_model.dart';
import 'package:flutter_boilerplate/src/resources/api_helper.dart';

class GlobalRepository {
  static final GlobalRepository _singleton = GlobalRepository._internal();

  factory GlobalRepository() {
    return _singleton;
  }

  GlobalRepository._internal();

  ApiHelper apiProvider = ApiHelper();

  ///STRING
  static const String GLOBAL_DEFAULT = '/8f361eb6-649a-4d8e-a685-8f9237bbf9a0';
  static const String FEED_LIST = '/7f8b281e-d112-407c-876c-715840d04dc0';

  ///FUNCTION
  //GET
  Future<GlobalDefaultResponseModel> globalDefault(
      {String? exampleParameter}) async {
    final request = GlobalDefaultResponseModel.generateParams(exampleParameter);
    final response = await apiProvider.get(GLOBAL_DEFAULT, params: request);
    final result = GlobalDefaultResponseModel.fromJson(response);
    return result;
  }

  //GET
  Future<FeedListResponseModel> feedList() async {
    final response = await apiProvider.get(FEED_LIST);
    final result = FeedListResponseModel.fromJson(response);
    return result;
  }
}
