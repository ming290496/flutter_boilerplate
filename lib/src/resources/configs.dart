import 'dart:io';

import 'package:flutter_boilerplate/src/constants/gl_enum.dart';

class Configs {
  /// IMPORTANT! CHECK BEFORE BUILD
  static const AppEnvironmentEnum appEnvironment = AppEnvironmentEnum.STAGING;

  // static const AppEnvironmentEnum appEnvironment = AppEnvironmentEnum.PRODUCTION;

  static const String _devBuildNumber = '.1';

  static const _baseUrlStaging = [
    'https://run.mocky.io/v3',
  ];

  static const _baseUrlProduction = [
    'https://run.mocky.io/v3',
  ];

  static String get getDevBuildNumber {
    String result = '';
    if (appEnvironment == AppEnvironmentEnum.STAGING) {
      result = _devBuildNumber;
    }
    return result;
  }

  static List<String> get baseUrl {
    List<String> result;
    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        result = _baseUrlStaging;
        break;
      case AppEnvironmentEnum.PRODUCTION:
        result = _baseUrlProduction;
        break;
    }
    return result;
  }

  static String get authorization {
    String result = '';

    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        if (Platform.isAndroid) {
          result = 'authorization_staging_android';
        } else {
          result = 'authorization_staging_ios';
        }
        break;
      case AppEnvironmentEnum.PRODUCTION:
        if (Platform.isAndroid) {
          result = 'authorization_production_android';
        } else {
          result = 'authorization_production_ios';
        }
        break;
    }
    return result;
  }

  static Duration get timeOutDuration {
    Duration result;

    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        result = Duration(seconds: 90);
        break;
      case AppEnvironmentEnum.PRODUCTION:
        result = Duration(minutes: 3);
        break;
    }
    return result;
  }

  // static AdjustConfig get adjustConfig {
  //   AdjustConfig config;
  //   switch (appEnvironment) {
  //     case AppEnvironmentEnum.STAGING:
  //       config = new AdjustConfig(
  //           AdjustDict.adjustKeyApp, AdjustEnvironment.sandbox);
  //       config.logLevel = AdjustLogLevel.verbose;
  //       break;
  //     case AppEnvironmentEnum.PRODUCTION:
  //       config =
  //           AdjustConfig(AdjustDict.adjustKeyApp, AdjustEnvironment.production);
  //       config.logLevel = AdjustLogLevel.suppress;
  //       break;
  //   }
  //   return config;
  // }

  static String get pushNotificationPrefix {
    String result = 'prod';
    switch (appEnvironment) {
      case AppEnvironmentEnum.STAGING:
        result = 'dev';
        break;
      case AppEnvironmentEnum.PRODUCTION:
        result = 'prod';
        break;
    }
    return result;
  }
}
